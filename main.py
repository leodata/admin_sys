"""
main.py - 主程序（项目入口）

Author: Hao
Date: 2021/9/9
"""
from datetime import timedelta

import flask
from flask_cors import CORS

from api import api_bp
from user import user_bp

app = flask.Flask(__name__)
CORS(app)

# 注册蓝图
app.register_blueprint(api_bp)
app.register_blueprint(user_bp)

# 修改Flask配置
# app.config['SECRET_KEY'] = 'Tl8sTUsNSzIjbjQkYDxGdmVoRmxHIWw8'
app.secret_key = 'Tl8sTUsNSzIjbjQkYDxGdmVoRmxHIWw8'
# app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(days=7)
app.permanent_session_lifetime = timedelta(days=7)


# @app.route('/')
# @check_login
# def show_index():
#     return redirect('/static/index.html')
#
#
# @app.errorhandler(404)
# def show_error_page(error):
#     return redirect('/static/lyear_pages_error.html')


# if __name__ == '__main__':
#     app.run(host='0.0.0.0', port=8000, debug=True)
